import subprocess, os
from pathlib import Path

from tests.unit_test import UnitTest


class TestInv(UnitTest):

    def test_inv_1(self):
        """parse parses a text invoice correctly"""
        from cue.invoice import Invoice
        filename = 'temp__Invoice__hollywood_muscle.txt'
        inv = Invoice.parse(Path(filename))
        assert 'total' in inv

    def test_inv_2(self):
        """issue assigns sequential number"""
        from cue.invoice import Invoice
        filename = 'temp__Invoice__hollywood_muscle.txt'
        inv = Invoice.parse(Path(filename))
        inv.issue()
        assert inv['number'] == 564

    def test_inv_3(self):
        """issue cleans up temp file"""
        from cue.invoice import Invoice
        filename = 'temp__Invoice__hollywood_muscle.txt'
        path = Path(filename)
        inv = Invoice.parse(path)
        inv.issue()
        try:
            path.read_text()
        except:
            assert True
        else:
            raise RuntimeError('tempfile was not cleaned up')

    def test_inv_4(self):
        """discount is handled correctly"""
        from cue.invoice import Invoice
        filename = 'temp__Invoice__american_cricket_goods.txt'
        inv = Invoice.parse(Path(filename))
        inv.issue()
        assert inv['total_ex'] == 1425000

    def test_inv_5(self):
        """boilerplate usa invoice has a Sales tax line"""
        from cue.invoice import Invoice
        Invoice.boilerplate('neo')
        path = Path('temp__Invoice__neo.txt')
        assert 'Sales tax' in path.read_text()

    def test_inv_6(self):
        """boilerplate aus invoice identifies as a tax invoice"""
        from cue.invoice import Invoice
        Invoice.boilerplate('agent_smith')
        path = Path('temp__Invoice__agent_smith.txt')
        assert 'TAX INVOICE' in path.read_text()

    def test_inv_7(self):
        """boilerplate simple invoice doesn't have an ex tax total"""
        from cue.invoice import Invoice
        Invoice.boilerplate('danny_archer')
        path = Path('temp__Invoice__danny_archer.txt')
        assert 'Total' not in path.read_text()
