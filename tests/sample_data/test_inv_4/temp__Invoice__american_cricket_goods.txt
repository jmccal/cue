Willow Wonder
Wood that really whacks
                                     INVOICE                            # 000001
                                                                Date: 2021-05-02
Bill to:
American Cricket Goods LLC
X555 Santa Monica Blvd
West Hollywood 90038

--------------------------------------------------------------------------------
Item code   Description                     Unit price      Qty            Price
--------------------------------------------------------------------------------
ss          willow 556                      100.00          1,000    $100,000.00
greynic     willow 356                      200.00          1,000    $200,000.00
mrf         willow 456                      300.00          1,000    $300,000.00
newbal      willow 5556                     400.00          1,000    $400,000.00
gm          willow 780                      500.00          1,000    $500,000.00
--------------------------------------------------------------------------------

                                                         Discount 5%
                                                         Sales tax   $106,875.00
    
                                                         TOTAL     $1,531,875.00


Terms: 17 days. Please include invoice number in remittance.
