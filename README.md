# cue

Accounting system for Linux

    $ mkdir my_awesome_startup
    $ cd my_awesome_startup
    $ cue init
    $ vi temp__Ledger__settings.yml

    ... edit settings as desired ...

    $ cue commit
    creating ledger for my_awesome_startup
    $ cue invoice apple
    $ vi temp__Invoice__apple.inv

    ... edit invoice as desired ...

    $ cue commit
