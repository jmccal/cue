@require(dollar_sign, total)
@(text = f'TOTAL{dollar_sign:>{23 - len(total)}}{total}')\
                                                    @text
